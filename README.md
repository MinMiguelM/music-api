# Music API

REST API which allows to create Songs with minimal information associated to an user.

## Table of contents

1. [Prerequisites](#prerequisites)
2. [Installation](#installation)
3. [Execution](#execution)
4. [Unit testing](#unit-testing)

## Prerequisites

- NodeJs 10.x or greater
- npm 6.x

## Installation

First of all, it's necessary to install the dependencies of the project, for installation run the next command in the root of the project.
- npm install

## Execution

Next command will run the server on port 4444
- npm start

Server is located in: http://localhost:4444

In case you want to change the port for this server, you just need to set an environment variable named `PORT`.

For example to run the server in port 8081:
- PORT=8081 npm start

Once the server is running, it loads a list of five songs to a memory database (sqlite3 was used for simplicity), which can be queried with an authenticated user.

The application has the next endpoints with its description and payload example.

---
### POST /api/auth/signup
---
Registration of users

Example of request payload:
```json
{
  "email": "mail@mail.com",
  "password": "Asdf12345?"
}
```

---
### POST /api/auth/login
---
Once user is registered, that user can authenticate to get a token and proceed with the next requests.

- Example of request payload:
```json
{
  "email": "mail@mail.com",
  "password": "Asdf12345?"
}
```

- Example of response:
```json
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNjMzNzA0NTE2LCJleHAiOjE2MzM3MDU3MTZ9.9HRxY3kwFCF53oeXd-_ppJsZE91FXjkoM6I0yJIKnMI
```

---
### POST /api/song
---
Creates a private song

- Authorization header must to be sent with a valid token

```json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNjMzNzA0NTE2LCJleHAiOjE2MzM3MDU3MTZ9.9HRxY3kwFCF53oeXd-_ppJsZE91FXjkoM6I0yJIKnMI
```

- Example of request payload:
```json
{
  "name": "Californication",
  "artist": "red hot chilipeppers"
}
```

- Example of response:
```json
{
  "id": 18,
  "name": "Californication",
  "artist": "red hot chilipeppers",
  "public": false,
  "userId": 2,
  "updatedAt": "2021-10-08T19:55:55.993Z",
  "createdAt": "2021-10-08T19:55:55.993Z"
}
```

---
### GET /api/song
---
Retrieves all private songs created by the authenticated user.

- This request uses pagination, therefore, if you want to traverse multiple pages, you need to use query param `page`. For example: `http://localhost:4444/api/song?page=2`
- Authorization header must to be sent with a valid token

```json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNjMzNzA0NTE2LCJleHAiOjE2MzM3MDU3MTZ9.9HRxY3kwFCF53oeXd-_ppJsZE91FXjkoM6I0yJIKnMI
```

- Example of response:
```json
{
  "metadata": {
    "page": 1,
    "numPages": 2,
    "numRecords": 12
  },
  "data": [...]
}
```

---
### PUT /api/song
---
Updates an existing song

- Authorization header must to be sent with a valid token

```json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNjMzNzA0NTE2LCJleHAiOjE2MzM3MDU3MTZ9.9HRxY3kwFCF53oeXd-_ppJsZE91FXjkoM6I0yJIKnMI
```

- Example of request payload:
```json
{
  "id": 1,
  "name": "Californication",
  "artist": "red hot chilipeppers"
}
```

- Example of response:
```json
{
  "id": 1,
  "name": "Californication",
  "artist": "red hot chilipeppers",
  "public": false,
  "userId": 2,
  "updatedAt": "2021-10-08T19:55:55.993Z",
  "createdAt": "2021-10-08T19:55:55.993Z"
}
```

---
### DELETE /api/song
---
Deletes all songs created by authenticated user

- Authorization header must to be sent with a valid token

```json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNjMzNzA0NTE2LCJleHAiOjE2MzM3MDU3MTZ9.9HRxY3kwFCF53oeXd-_ppJsZE91FXjkoM6I0yJIKnMI
```

---
### DELETE /api/song/:songId
---
Deletes a specific private song created by authenticated user.

- Authorization header must to be sent with a valid token

```json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNjMzNzA0NTE2LCJleHAiOjE2MzM3MDU3MTZ9.9HRxY3kwFCF53oeXd-_ppJsZE91FXjkoM6I0yJIKnMI
```

---
### GET /api/song/public
---
Retrieves all public songs.

- This request uses pagination, therefore, if you want to traverse multiple pages, you need to use query param `page`. For example: `http://localhost:4444/api/song/public?page=2`
- Authorization header must to be sent with a valid token

```json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNjMzNzA0NTE2LCJleHAiOjE2MzM3MDU3MTZ9.9HRxY3kwFCF53oeXd-_ppJsZE91FXjkoM6I0yJIKnMI
```

- Example of response:
```json
{
  "metadata": {
    "page": 1,
    "numPages": 2,
    "numRecords": 12
  },
  "data": [...]
}
```

---
### POST /api/song/:songId/like
---
Likes a public song.

- Authorization header must to be sent with a valid token

```json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNjMzNzA0NTE2LCJleHAiOjE2MzM3MDU3MTZ9.9HRxY3kwFCF53oeXd-_ppJsZE91FXjkoM6I0yJIKnMI
```

- Example of response:
```json
{
  "id": 3,
  "userId": 2,
  "songId": "2",
  "updatedAt": "2021-10-08T14:48:46.271Z",
  "createdAt": "2021-10-08T14:48:46.271Z"
}
```

---
### GET /api/song/likes
---
Retrieves liked songs by the authenticated user.

- This request uses pagination, therefore, if you want to traverse multiple pages, you need to use query param `page`. For example: `http://localhost:4444/api/song/likes?page=2`
- Authorization header must to be sent with a valid token

```json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNjMzNzA0NTE2LCJleHAiOjE2MzM3MDU3MTZ9.9HRxY3kwFCF53oeXd-_ppJsZE91FXjkoM6I0yJIKnMI
```

- Example of response:
```json
{
  "metadata": {
    "page": 1,
    "numPages": 2,
    "numRecords": 12
  },
  "data": [...]
}
```

---
### GET /api/song/:songId/lyrics
---
Retrieves the lyrics of a public or private song created by authenticated user.

> This endpoint connects to an external API `http://api.lyrics.ovh`, to retrieve the lyrics of a song.

- This request uses pagination, therefore, if you want to traverse multiple pages, you need to use query param `page`. For example: `http://localhost:4444/api/song/likes?page=2`
- Authorization header must to be sent with a valid token

```json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNjMzNzA0NTE2LCJleHAiOjE2MzM3MDU3MTZ9.9HRxY3kwFCF53oeXd-_ppJsZE91FXjkoM6I0yJIKnMI
```

- Example of response:
```json
{
  "lyrics": "Paroles de la chanson Heat Above par Greta Van Fleet\r\n[Josh Kiszka]\nSorrows of the Earth\nMay our tears of rain wash down to bathe you\nThis is what life is worth\nWhen the fires still burn and rage all around\n\n[Josh Kiszka]\nCan you hear that dreadful sound?\nFire still burning on the ground\n\n[Josh Kiszka]"
}
```

## Unit testing

Command for running unit testing:
- npm test

There are only two components tested, one service and one controller. I chose them because they contain the most important logic requested in the homework.
const createError = require('http-errors');
const express = require('express');
const app = express();
const db = require("./src/models");
const SongModel = require("./src/models/song.model");
const authMiddleware = require("./src/middlewares/auth.middleware");
const initialData = require("./src/config/initialData.json");

const authController = require("./src/controllers/auth.controller");
const songController = require("./src/controllers/song.controller");

app.use(express.json({limit: '50mb'}));

app.get('/', (req, res) => {
  res.send("Music API")
});

app.use('/api/auth', authController);
app.use('/api/song', authMiddleware.verifyToken, songController);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404, "Page not found"));
});

// error handler
app.use(function(err, req, res, next) {
  if (req.app.get('env') === 'development' || req.app.get('env') === 'production') {
    console.log(err);
  }
  // render the error page
  res.status(err.status || 500);
  res.json({
    status: err.status || 500,
    message: err.message
  });
});

db.sync({ alter: true }).then(async () => {
  const totalSongs = await SongModel.count({});
  if (totalSongs === 0) {
    console.log("Creating public songs...")
    initialData.forEach(async (song) => {
      await SongModel.create(song)
    })
  }
})

module.exports = app;
const config = () => {
  if (process.env.NODE_ENV === "production") {
    return {
      jwtSecret: "qk4u354;lo24jj3l;4j2"
    }
  }
  // dev
  return {
    jwtSecret: "secret"
  }
}

module.exports = config();
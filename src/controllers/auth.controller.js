const express = require('express');
const createError = require('http-errors');
const router = express.Router();
const ControlledError = require('../errors/controlled.error');
const AuthService = require("../services/auth.service");
const UserService = require('../services/user.service');

const userService = new UserService();
const authService = new AuthService(userService);

router.post('/signup', async (req, res, next) => {
  try {
    const user = req.body;
    if (!user.email || !user.password) {
      return next(createError(400, "email and password are mandatory fields"));
    }
    await authService.signUp(user);
    res.sendStatus(201);
  } catch (err) {
    if (err instanceof ControlledError) {
      return next(createError(400, err.message));
    }
    next(createError(500, err.message));
  }
})

router.post('/login', async (req, res, next) => {
  try {
    const user = req.body;
    if (!user.email || !user.password) {
      return next(createError(400, "email and password are mandatory fields"));
    }
    const token = await authService.login(user);
    if (token) {
      return res.status(200).send(token);
    }
    next(createError(401))
  } catch (err) {
    next(createError(500, err.message));
  }
})

module.exports = router
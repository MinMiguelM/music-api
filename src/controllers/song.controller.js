const express = require('express');
const createError = require('http-errors');
const router = express.Router();
const ControlledError = require('../errors/controlled.error');
const SongService = require("../services/song.service");

const songService = new SongService();

router.get('/', async (req, res, next) => {
  try {
    const page = req.query["page"] || 1;
    const songs = await songService.getAll(req.user, page);
    res.status(200).send(songs);
  } catch (err) {
    if (err instanceof ControlledError) {
      return next(createError(400, err.message));
    }
    next(createError(500, err.message));
  }
});

router.get('/public', async (req, res, next) => {
  try {
    const page = req.query["page"] || 1;
    const songs = await songService.getAllPublic(page);
    res.status(200).send(songs);
  } catch (err) {
    if (err instanceof ControlledError) {
      return next(createError(400, err.message));
    }
    next(createError(500, err.message));
  }
});

router.get('/likes', async (req, res, next) => {
  try {
    const page = req.query["page"] || 1;
    const songs = await songService.getAllLikedSongs(req.user, page);
    res.status(200).send(songs);
  } catch (err) {
    if (err instanceof ControlledError) {
      return next(createError(400, err.message));
    }
    next(createError(500, err.message));
  }
});

router.post('/', async (req, res, next) => {
  try {
    const song = req.body;
    if (!song.name || !song.artist) {
      return next(createError(400, "Name and artist fields are required"))
    }
    const newSong = await songService.createSong(req.user, song);
    res.status(201).send(newSong);
  } catch (err) {
    if (err instanceof ControlledError) {
      return next(createError(400, err.message));
    }
    next(createError(500, err.message));
  }
})

router.delete('/', async (req, res, next) => {
  try {
    await songService.deleteAllSongs(req.user);
    res.sendStatus(204);
  } catch (err) {
    if (err instanceof ControlledError) {
      return next(createError(400, err.message));
    }
    next(createError(500, err.message));
  }
})

router.delete('/:songId', async (req, res, next) => {
  try {
    const result = await songService.deleteById(req.user, req.params.songId);
    if (result) {
      return res.sendStatus(204);
    }
    next(createError(404));
  } catch (err) {
    if (err instanceof ControlledError) {
      return next(createError(400, err.message));
    }
    next(createError(500, err.message));
  }
})

router.put('/', async (req, res, next) => {
  try {
    const song = req.body;
    if (!song.id) {
      return next(createError(400, "ID is required"))
    }
    const updatedSong = await songService.updateSong(req.user, song);
    if (updatedSong) {
      return res.status(200).send(updatedSong);
    }
    next(createError(404));
  } catch (err) {
    if (err instanceof ControlledError) {
      return next(createError(400, err.message));
    }
    next(createError(500, err.message));
  }
})

router.post('/:songId/like', async (req, res, next) => {
  try {
    const like = await songService.likeSong(req.user, req.params.songId);
    if (like) {
      return res.status(201).send(like);
    }
    next(createError(404));
  } catch (err) {
    if (err instanceof ControlledError) {
      return next(createError(400, err.message));
    }
    next(createError(500, err.message));
  }
})

router.get('/:songId/lyrics', async (req, res, next) => {
  try {
    const lyrics = await songService.getLyrics(req.user, req.params.songId);
    if (lyrics) {
      return res.status(200).send(lyrics);
    }
    next(createError(404));
  } catch (err) {
    if (err instanceof ControlledError) {
      return next(createError(400, err.message));
    }
    next(createError(500, err.message));
  }
})

module.exports = router
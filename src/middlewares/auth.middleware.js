const createError = require('http-errors');
const jwt = require("jsonwebtoken");
const config = require("../config");

const verifyToken = async (req, res, next) => {
  const header = req.headers["authorization"];
  if (header) {
    let token = header.split(' ')
    if (token.length > 1) {
      token = token[1];
      return jwt.verify(token, config.jwtSecret, (err, decoded) => {
        if (err) {
          return next(createError(401, err.message))
        }
        req.user = { id: decoded.id } 
        next();
      })
    }
  }
  next(createError(401))
}

module.exports = {
  verifyToken,
}
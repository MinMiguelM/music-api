const {DataTypes} = require('sequelize');
const sequelize = require('.');

const Like = sequelize.define('like', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  songId: {
    type: DataTypes.INTEGER,
  },
  userId: {
    type: DataTypes.INTEGER,
  },
});

module.exports = Like;
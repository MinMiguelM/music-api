const {DataTypes} = require('sequelize');
const sequelize = require('.');

const Song = sequelize.define('song', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  public: DataTypes.BOOLEAN,
  name: DataTypes.STRING,
  year: DataTypes.INTEGER,
  duration: DataTypes.INTEGER,
  artist: DataTypes.STRING,
  album: DataTypes.STRING,
  userId: DataTypes.INTEGER,
});

module.exports = Song;
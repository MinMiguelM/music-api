const EmailValidator = require("email-validator");
const ControlledError = require('../errors/controlled.error');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const config = require("../config");

class AuthService {
  constructor(userService) {
    this._userService = userService;
    this._passwordRegex = new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#?\]])[A-Za-z\d!@#?\]]{10,}$/);
  }

  async signUp({email, password}) {
    this._isEmailValid(email);
    this._isPasswordValid(password);
    password = this._hashPassword(password);
    const user = {
      email, password
    }
    await this._userService.createUser(user);
  }

  async login({email, password}) {
    this._isEmailValid(email);
    this._isPasswordValid(password);
    const user = await this._userService.getUserByEmail(email);
    if (user && bcrypt.compareSync(password, user.password)) {
      return jwt.sign({ id: user.id }, config.jwtSecret, { expiresIn: 60 * 20 });
    }
    return null;
  }

  _isEmailValid(email) {
    if (!EmailValidator.validate(email)) {
      throw new ControlledError("Invalid email");
    }
  }

  _isPasswordValid(password) {
    if (!this._passwordRegex.test(password)) {
      throw new ControlledError("Password must contains at least 10 characters, one lowercase letter, one uppercase letter and one of the following characters: !, @, #, ? or ].")
    }
  }

  _hashPassword(password) {
    return bcrypt.hashSync(password, 8);
  }
}

module.exports = AuthService;
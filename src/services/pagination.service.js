const ControlledError = require("../errors/controlled.error");

class PaginationService {
  constructor() {
    this.ELEMENTS_PER_PAGE = 10;
  }

  async paginate(model, query, page) {
    if (page <= 0) {
      throw new ControlledError("Page number must to be greater than 0");
    }
    const total = await model.count(query);
    const numPages = Math.ceil(total / this.ELEMENTS_PER_PAGE) || 1;
    if (page > numPages) {
      throw new ControlledError("Page number has been exceeded")
    }
    const offset = (page - 1) * this.ELEMENTS_PER_PAGE;
    const data = await model.findAll({
      ...query,
      limit: this.ELEMENTS_PER_PAGE,
      offset
    });
    return {
      metadata: {
        page,
        numPages,
        numRecords: total
      },
      data
    }
  }
}

module.exports = PaginationService;
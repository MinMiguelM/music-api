const { Op } = require("sequelize");
const SongModel = require("../models/song.model");
const LikeModel = require("../models/like.model");
const ControlledError = require("../errors/controlled.error");
const axios = require("axios");
const PaginationService = require("./pagination.service");

class SongService {
  constructor() {
    this._paginationService = new PaginationService();
  }

  async getAll(user, page) {
    return await this._paginationService.paginate(SongModel, { where: { userId: user.id } }, page);
  }

  async getAllPublic(page) {
    return await this._paginationService.paginate(SongModel, {where: { public: true }}, page);
  }

  async createSong(user, song) {
    const newSong = await SongModel.create({
      name: song.name,
      artist: song.artist,
      public: false,
      year: song.year,
      duration: song.duration,
      album: song.album,
      userId: user.id,
    });
    return newSong;
  }

  async deleteAllSongs(user) {
    await SongModel.destroy({
      where: {
        userId: user.id,
        public: false,
      }
    });
  }

  async deleteById(user, songId) {
    const song = await SongModel.findOne({
      where: {
        id: songId,
      }
    });
    if (song) {
      if (song.userId !== user.id) {
        throw new ControlledError("Song belongs to another user.")
      }
      await song.destroy();
      return true;
    }
    return false;
  }

  async likeSong(user, songId) {
    const song = await SongModel.findOne({
      where: {
        id: songId
      }
    });
    if (!song) {
      return null;
    }
    if (!song.public) {
      throw new ControlledError("Song is not public");
    }
    const like = await LikeModel.findOne({
      where: {
        userId: user.id,
        songId
      }
    });
    if (like) {
      throw new ControlledError("Song was already liked");
    }
    return await LikeModel.create({ userId: user.id, songId })
  }

  async getAllLikedSongs(user, page) {
    const songIds = await this._paginationService.paginate(LikeModel, {
      attributes: ['songId'],
      where: {
        userId: user.id
      }
    }, page);
    let ids = []
    songIds.data.map((song) => ids.push(song.songId));
    const songs = await SongModel.findAll({
      where: {
        id: { [Op.in]: ids }
      }
    })
    return {
      metadata: songIds.metadata,
      data: songs
    }
  }

  async updateSong(user, song) {
    const modelSong = await SongModel.findOne({
      where: {
        id: song.id
      }
    });
    if (!modelSong) return null;
    if (modelSong.userId !== user.id) {
      throw new ControlledError("Song belongs to another user.")
    }
    modelSong.name = song.name || modelSong.name;
    modelSong.artist = song.artist || modelSong.artist;
    modelSong.year = song.year || modelSong.year;
    modelSong.duration = song.duration || modelSong.duration;
    modelSong.album = song.album || modelSong.album
    return await modelSong.save()
  }

  async getLyrics(user, songId) {
    const modelSong = await SongModel.findOne({
      where: {
        id: songId
      }
    });
    if (!modelSong) return null;
    if (modelSong.userId !== user.id && !modelSong.public) {
      throw new ControlledError("Song belongs to another user.")
    }
    if (!modelSong.artist || !modelSong.name) {
      throw new ControlledError("Song must to have name and artist values.")
    }
    const response = await axios.get(`http://api.lyrics.ovh/v1/${modelSong.artist}/${modelSong.name}`);
    return response.data
  }
}

module.exports = SongService;
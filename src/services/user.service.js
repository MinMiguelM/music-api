const { UniqueConstraintError } = require('sequelize');
const ControlledError = require('../errors/controlled.error');
const UserModel = require("../models/user.model");

class UserService {
  constructor() {
  }

  async createUser(user) {
    try {
      await UserModel.create(user)
    } catch (err) {
      if (err instanceof UniqueConstraintError) {
        throw new ControlledError("Email already registered");
      }
      throw err;
    }
  }

  async getUserByEmail(email) {
    return await UserModel.findOne({ where: { email } });
  }
}

module.exports = UserService;
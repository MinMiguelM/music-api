const sinon = require("sinon");
const assert = require('chai').assert;
const bcrypt = require("bcrypt");
const AuthService = require("../src/services/auth.service");
const ControlledError = require('../src/errors/controlled.error');

describe("Auth service", () => {
  let userServiceMock,
    authService;

  beforeEach(() => {
    userServiceMock = sinon.stub({
      createUser: Function(),
      getUserByEmail: Function()
    })
    authService = new AuthService(userServiceMock);
  })

  it("Creating user: success", async () => {
    userServiceMock.createUser.returns(undefined);
    await authService.signUp({email: "mail@mail.com", password: "Asdf123456!"})
  })

  it("Creating user: error email invalid", async () => {
    userServiceMock.createUser.returns(undefined);
    try {
      await authService.signUp({email: "mail@mail", password: "Asdf123456!"})
    } catch (err) {
      assert.instanceOf(err, ControlledError)
      assert.strictEqual(err.message, "Invalid email")
      return;
    }
    assert.fail("should throw an exception")
  })

  it("Creating user: error password less than 10 characters", async () => {
    userServiceMock.createUser.returns(undefined);
    try {
      await authService.signUp({email: "mail@mail.com", password: "Asdf1234!"})
    } catch (err) {
      assert.instanceOf(err, ControlledError)
      assert.strictEqual(err.message, "Password must contains at least 10 characters, one lowercase letter, one uppercase letter and one of the following characters: !, @, #, ? or ].")
      return;
    }
    assert.fail("should throw an exception")
  })

  it("Creating user: error password does not contain uppercase", async () => {
    userServiceMock.createUser.returns(undefined);
    try {
      await authService.signUp({email: "mail@mail.com", password: "asdf123456!"})
    } catch (err) {
      assert.instanceOf(err, ControlledError)
      assert.strictEqual(err.message, "Password must contains at least 10 characters, one lowercase letter, one uppercase letter and one of the following characters: !, @, #, ? or ].")
      return;
    }
    assert.fail("should throw an exception")
  })

  it("Creating user: error password does not contain lowercase", async () => {
    userServiceMock.createUser.returns(undefined);
    try {
      await authService.signUp({email: "mail@mail.com", password: "ASDF123456!"})
    } catch (err) {
      assert.instanceOf(err, ControlledError)
      assert.strictEqual(err.message, "Password must contains at least 10 characters, one lowercase letter, one uppercase letter and one of the following characters: !, @, #, ? or ].")
      return;
    }
    assert.fail("should throw an exception")
  })

  it("Creating user: error password does not contain numbers", async () => {
    userServiceMock.createUser.returns(undefined);
    try {
      await authService.signUp({email: "mail@mail.com", password: "Asdfsdfskdfh!"})
    } catch (err) {
      assert.instanceOf(err, ControlledError)
      assert.strictEqual(err.message, "Password must contains at least 10 characters, one lowercase letter, one uppercase letter and one of the following characters: !, @, #, ? or ].")
      return;
    }
    assert.fail("should throw an exception")
  })

  it("Creating user: error password does not contain special characters", async () => {
    userServiceMock.createUser.returns(undefined);
    try {
      await authService.signUp({email: "mail@mail.com", password: "Asdf1234567"})
    } catch (err) {
      assert.instanceOf(err, ControlledError)
      assert.strictEqual(err.message, "Password must contains at least 10 characters, one lowercase letter, one uppercase letter and one of the following characters: !, @, #, ? or ].")
      return;
    }
    assert.fail("should throw an exception")
  })

  it("Creating user: error email is already registered", async () => {
    userServiceMock.createUser.throws(new ControlledError("Email already registered"));
    try {
      await authService.signUp({email: "mail@mail.com", password: "Asdf123456!"})
    } catch (err) {
      assert.instanceOf(err, ControlledError)
      assert.strictEqual(err.message, "Email already registered")
      return;
    }
    assert.fail("should throw an exception")
  })

  it("Login: success", async () => {
    userServiceMock.getUserByEmail.withArgs("mail@mail.com").returns({
      id: 1,
      password: hashPassword("Asdf123456!")
    });
    const token = await authService.login({email: "mail@mail.com", password: "Asdf123456!"});
    assert.isNotNull(token);
  })

  it("Login: error passwords do not match", async () => {
    userServiceMock.getUserByEmail.withArgs("mail@mail.com").returns({
      id: 1,
      password: hashPassword("Asdf123456")
    });
    const token = await authService.login({email: "mail@mail.com", password: "Asdf123456!"});
    assert.isNull(token);
  })

  it("Login: error cant find user", async () => {
    userServiceMock.getUserByEmail.withArgs("mail@mail.com").returns(null);
    const token = await authService.login({email: "mail@mail.com", password: "Asdf123456!"});
    assert.isNull(token);
  })

  const hashPassword = (password) => {
    return bcrypt.hashSync(password, 8);
  }
})
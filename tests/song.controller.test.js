const supertest = require("supertest");
const sinon = require("sinon");
const jwt = require('jsonwebtoken');
const ControlledError = require('../src/errors/controlled.error');
const config = require('../src/config');

describe('Song controller', () => {
  let SongService,
    songServiceMock,
    app;

  beforeEach(() => {
    SongService = require('../src/services/song.service');
    app = require('../index');
  })

  afterEach(() => {
    songServiceMock.restore();
  })

  it('Get all private songs: success', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "getAll")
    const response = {
      "metadata": {
        "page": "1",
        "numPages": 1,
        "numRecords": 1
      },
      "data": [{
        "id": 17,
        "public": false,
        "name": "bichota",
        "year": null,
        "duration": null,
        "artist": "karol",
        "album": null,
        "userId": 2,
        "createdAt": "2021-10-08T13:56:01.705Z",
        "updatedAt": "2021-10-08T13:56:01.705Z"
      }]
    }
    songServiceMock.withArgs({ id: 1 }, 1).returns(response)
    const token = generateToken();
    await supertest(app)
      .get('/api/song')
      .set('Authorization', `bearer ${token}`)
      .expect(200, response)
  })

  it('Get all private songs: error', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "getAll")
    songServiceMock.withArgs({ id: 1 }, 1).throws(new ControlledError("Invalid Page number"))
    const token = generateToken();
    await supertest(app)
      .get('/api/song')
      .set('Authorization', `bearer ${token}`)
      .expect(400, {
        status: 400,
        message: "Invalid Page number"
      })
  })

  it('Unauthorized request', async () => {
    await supertest(app)
      .get('/api/song')
      .expect(401)
  })

  it('Get all public songs: success', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "getAllPublic")
    const response = {
      "metadata": {
        "page": "1",
        "numPages": 1,
        "numRecords": 1
      },
      "data": [{
        "id": 17,
        "public": true,
        "name": "bichota",
        "year": null,
        "duration": null,
        "artist": "karol",
        "album": null,
        "userId": null,
        "createdAt": "2021-10-08T13:56:01.705Z",
        "updatedAt": "2021-10-08T13:56:01.705Z"
      }]
    }
    songServiceMock.withArgs(1).returns(response)
    const token = generateToken();
    await supertest(app)
      .get('/api/song/public')
      .set('Authorization', `bearer ${token}`)
      .expect(200, response)
  })

  it('Get all public songs: error', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "getAllPublic")
    songServiceMock.withArgs(1).throws(new ControlledError("Invalid Page number"))
    const token = generateToken();
    await supertest(app)
      .get('/api/song/public')
      .set('Authorization', `bearer ${token}`)
      .expect(400, {
        status: 400,
        message: "Invalid Page number"
      })
  })

  it('Get all liked songs: success', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "getAllLikedSongs")
    const response = {
      "metadata": {
        "page": "1",
        "numPages": 1,
        "numRecords": 1
      },
      "data": [{
        "id": 17,
        "public": true,
        "name": "bichota",
        "year": null,
        "duration": null,
        "artist": "karol",
        "album": null,
        "userId": null,
        "createdAt": "2021-10-08T13:56:01.705Z",
        "updatedAt": "2021-10-08T13:56:01.705Z"
      }]
    }
    songServiceMock.withArgs({ id: 1 }, 1).returns(response)
    const token = generateToken();
    await supertest(app)
      .get('/api/song/likes')
      .set('Authorization', `bearer ${token}`)
      .expect(200, response)
  })

  it('Get all liked songs: error', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "getAllLikedSongs")
    songServiceMock.withArgs({ id: 1 }, 1).throws(new ControlledError("Invalid Page number"))
    const token = generateToken();
    await supertest(app)
      .get('/api/song/likes')
      .set('Authorization', `bearer ${token}`)
      .expect(400, {
        status: 400,
        message: "Invalid Page number"
      })
  })

  it('Create song: error require fields', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "createSong")
    const song = {
      name: 'Californication'
    }
    const token = generateToken();
    await supertest(app)
      .post('/api/song')
      .send(song)
      .set('Authorization', `bearer ${token}`)
      .expect(400, {
        status: 400,
        message: "Name and artist fields are required"
      })
  })

  it('Create song: success', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "createSong")
    const song = {
      name: 'Californication',
      artist: 'Red hot chilipeppers'
    }
    songServiceMock.withArgs({ id: 1 }, song).returns({ ...song, id: 1 })
    const token = generateToken();
    await supertest(app)
      .post('/api/song')
      .send(song)
      .set('Authorization', `bearer ${token}`)
      .expect(201, { ...song, id: 1 })
  })

  it('Delete all private songs: success', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "deleteAllSongs")
    songServiceMock.withArgs({ id: 1 }).returns(undefined)
    const token = generateToken();
    await supertest(app)
      .delete('/api/song')
      .set('Authorization', `bearer ${token}`)
      .expect(204)
  })

  it('Delete a specific song: success', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "deleteById")
    songServiceMock.withArgs({ id: 1 }, "1").returns(true)
    const token = generateToken();
    await supertest(app)
      .delete('/api/song/1')
      .set('Authorization', `bearer ${token}`)
      .expect(204)
  })

  it('Delete a specific song: error not found', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "deleteById")
    songServiceMock.withArgs({ id: 1 }, "1").returns(false)
    const token = generateToken();
    await supertest(app)
      .delete('/api/song/1')
      .set('Authorization', `bearer ${token}`)
      .expect(404)
  })

  it('Delete a specific song: error song belongs to another one', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "deleteById")
    songServiceMock.withArgs({ id: 1 }, "1").throws(new ControlledError("Song belongs to another user."))
    const token = generateToken();
    await supertest(app)
      .delete('/api/song/1')
      .set('Authorization', `bearer ${token}`)
      .expect(400, {
        status: 400,
        message: "Song belongs to another user."
      })
  })

  it('Update a specific song: success', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "updateSong");
    const song = {
      id: 1,
      name: 'Californication',
      artist: 'Red hot chilipeppers'
    }
    songServiceMock.withArgs({ id: 1 }, song).returns(song);
    const token = generateToken();
    await supertest(app)
      .put('/api/song')
      .send(song)
      .set('Authorization', `bearer ${token}`)
      .expect(200, song)
  })

  it('Update a specific song: error id is required', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "updateSong");
    const song = {
      name: 'Californication',
      artist: 'Red hot chilipeppers'
    }
    songServiceMock.withArgs({ id: 1 }, song).returns(song);
    const token = generateToken();
    await supertest(app)
      .put('/api/song')
      .send(song)
      .set('Authorization', `bearer ${token}`)
      .expect(400, {
        status: 400,
        message: "ID is required"
      })
  })

  it('Update a specific song: error id not found', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "updateSong");
    const song = {
      id: 2,
      name: 'Californication',
      artist: 'Red hot chilipeppers'
    }
    songServiceMock.withArgs({ id: 1 }, song).returns(undefined);
    const token = generateToken();
    await supertest(app)
      .put('/api/song')
      .send(song)
      .set('Authorization', `bearer ${token}`)
      .expect(404)
  })

  it('Update a specific song: error song belongs to another one', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "updateSong");
    const song = {
      id: 2,
      name: 'Californication',
      artist: 'Red hot chilipeppers'
    }
    songServiceMock.withArgs({ id: 1 }, song).throws(new ControlledError("Song belongs to another user."));
    const token = generateToken();
    await supertest(app)
      .put('/api/song')
      .send(song)
      .set('Authorization', `bearer ${token}`)
      .expect(400, {
        status: 400,
        message: "Song belongs to another user."
      })
  })

  it('Like a public song: success', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "likeSong");
    const response = {
      "id": 3,
      "userId": 2,
      "songId": "2",
    }
    songServiceMock.withArgs({ id: 1 }, "1").returns(response);
    const token = generateToken();
    await supertest(app)
      .post('/api/song/1/like')
      .set('Authorization', `bearer ${token}`)
      .expect(201, response)
  })

  it('Like a public song: error not found', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "likeSong");
    songServiceMock.withArgs({ id: 1 }, "1").returns(undefined);
    const token = generateToken();
    await supertest(app)
      .post('/api/song/1/like')
      .set('Authorization', `bearer ${token}`)
      .expect(404)
  })

  it('Like a public song: error song is not public', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "likeSong");
    songServiceMock.withArgs({ id: 1 }, "1").throws(new ControlledError("Song is not public"));
    const token = generateToken();
    await supertest(app)
      .post('/api/song/1/like')
      .set('Authorization', `bearer ${token}`)
      .expect(400, {
        status: 400,
        message: "Song is not public"
      })
  });

  it('Get lyrics of a song: success', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "getLyrics");
    const response = {
      "lyrics": "Paroles de la chanson Bichota par Karol G\r\nSalgo acicalá' de"
    }
    songServiceMock.withArgs({ id: 1 }, "1").returns(response);
    const token = generateToken();
    await supertest(app)
      .get('/api/song/1/lyrics')
      .set('Authorization', `bearer ${token}`)
      .expect(200, response)
  });

  it('Get lyrics of a song: error not found', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "getLyrics");
    songServiceMock.withArgs({ id: 1 }, "1").returns(undefined);
    const token = generateToken();
    await supertest(app)
      .get('/api/song/1/lyrics')
      .set('Authorization', `bearer ${token}`)
      .expect(404)
  });

  it('Get lyrics of a song: error song belongs to another one', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "getLyrics");
    songServiceMock.withArgs({ id: 1 }, "1").throws(new ControlledError("Song belongs to another user"));
    const token = generateToken();
    await supertest(app)
      .get('/api/song/1/lyrics')
      .set('Authorization', `bearer ${token}`)
      .expect(400, {
        status: 400,
        message: "Song belongs to another user"
      })
  });

  it('Get lyrics of a song: error name and artists are required fields', async () => {
    songServiceMock = sinon.stub(SongService.prototype, "getLyrics");
    songServiceMock.withArgs({ id: 1 }, "1").throws(new ControlledError("Song must to have name and artist values."));
    const token = generateToken();
    await supertest(app)
      .get('/api/song/1/lyrics')
      .set('Authorization', `bearer ${token}`)
      .expect(400, {
        status: 400,
        message: "Song must to have name and artist values."
      })
  });

  const generateToken = () => {
    return jwt.sign({ id: 1 }, config.jwtSecret, { expiresIn: 60 * 20 });
  }
})